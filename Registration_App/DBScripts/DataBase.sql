--Scripts Assignment


USE [A00424877]
GO

/****** Object:  Table [dbo].[Hotels]    Script Date: 2018-02-28 10:03:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Hotels](
	[IDHotel] [int] IDENTITY(1,1) NOT NULL,
	[HotelName] [varchar](50) NULL,
	[PhoneNumber] [nchar](10) NULL,
	[Email] [nchar](10) NULL,
	[Location] [varchar](50) NULL,
 CONSTRAINT [PK__Hotels__64D8806E05BA38B8] PRIMARY KEY CLUSTERED 
(
	[IDHotel] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--------------------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[Customer]    Script Date: 2018-02-28 10:11:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Customer](
	[IDClient] [int] IDENTITY(1,1) NOT NULL,
	[FistName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[StreetNumber] [varchar](200) NULL,
	[City] [varchar](50) NULL,
	[Province] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[PostalCode] [varchar](20) NULL,
	[PhoneNum] [int] NULL,
	[Email] [varchar](50) NULL,
 CONSTRAINT [PK__Customer__CDECAB2C859D1C43] PRIMARY KEY CLUSTERED 
(
	[IDClient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
---------------------------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[CreditCards]    Script Date: 2018-02-28 10:12:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CreditCards](
	[IDCard] [int] IDENTITY(1,1) NOT NULL,
	[ClientID] [int] NOT NULL,
	[CreditType] [varchar](2) NULL,
	[CCName] [varchar](50) NULL,
	[CCNumber] [int] NULL,
	[CCExpDate] [date] NULL,
 CONSTRAINT [PK__CreditCa__43A2A4E24E511FE4] PRIMARY KEY CLUSTERED 
(
	[IDCard] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CreditCards]  WITH CHECK ADD  CONSTRAINT [fk_client_id] FOREIGN KEY([ClientID])
REFERENCES [dbo].[Customer] ([IDClient])
GO

ALTER TABLE [dbo].[CreditCards] CHECK CONSTRAINT [fk_client_id]
GO

-----------------------------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[Reservations]    Script Date: 2018-02-28 10:13:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Reservations](
	[IDReservation] [int] NOT NULL,
	[ClientID] [int] NULL,
	[CheckinDate] [date] NULL,
	[CheckoutDAte] [date] NULL,
	[GuesNum] [int] NULL,
	[RoomsNum] [int] NULL,
	[GuestNum] [int] NULL,
 CONSTRAINT [PK_Reservations] PRIMARY KEY CLUSTERED 
(
	[IDReservation] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Reservations]  WITH CHECK ADD  CONSTRAINT [fk_re_client_id] FOREIGN KEY([ClientID])
REFERENCES [dbo].[Customer] ([IDClient])
GO

ALTER TABLE [dbo].[Reservations] CHECK CONSTRAINT [fk_re_client_id]
GO
------------------------------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[Users]    Script Date: 2018-02-28 10:13:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Users](
	[IDClient] [int] NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [varchar](30) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [fk_usr_client_id] FOREIGN KEY([IDClient])
REFERENCES [dbo].[Customer] ([IDClient])
GO

ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [fk_usr_client_id]
GO

