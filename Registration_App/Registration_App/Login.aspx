﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Registration_App.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body {
            background-image:none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style ="margin:auto;border:5px solid white">
                <tr>
                    <td>
                         <asp:Label ID="Label1" runat="server" Text="UserName"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TBxUsrName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                         <asp:Label ID="Label2" runat="server" Text="Password"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TBxPasswd" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                         
                    </td>
                    <td>
                        <asp:Button ID="BTLogin" runat="server" Text="Login" />
                    </td>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="User or Password Incorrect"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
